package com.example;

import javax.sql.DataSource;
import java.sql.SQLException;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;

@SpringBootTest
public class SpringBootJDBCTest {
    @Autowired
    DataSource dataSource;  // 数据源组件
    @Autowired
    JdbcTemplate jdbcTemplate;  // 用于访问数据库的组件

    @Test
    void contextLoads() throws SQLException {
        System.out.println("DataSource：" + dataSource.getClass());
        System.out.println("DataSource Connection：" + dataSource.getConnection());

        Integer i = jdbcTemplate.queryForObject("SELECT count(*) from `user`", Integer.class);
        System.out.println("Table user has " + i + " record");
    }
}
