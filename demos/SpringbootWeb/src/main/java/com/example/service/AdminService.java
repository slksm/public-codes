package com.example.service;

import com.example.entity.Admin;

public interface AdminService {
    public Admin getByNameAndPassword(Admin admin);
}