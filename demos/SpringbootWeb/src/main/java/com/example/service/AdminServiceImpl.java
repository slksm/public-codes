package com.example.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.entity.Admin;
import com.example.mapper.AdminMapper;
import com.example.service.AdminService;

@Service("adminService")
public class AdminServiceImpl implements AdminService {
    @Autowired
    AdminMapper adminMapper;

    @Override
    public Admin getByNameAndPassword(Admin admin) {
        return adminMapper.getByNameAndPassword(admin);
    }
}