package com.example.config;

import java.util.Arrays;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.example.web.TestServlet;
import com.example.web.TestFiler;
import com.example.web.TestListener;

@Configuration
public class TestBeanConfig {

    @Bean
    public ServletRegistrationBean servletRegistrationBean() {
        TestServlet testServlet = new TestServlet();
        return new ServletRegistrationBean(testServlet, "/testServlet");
    }

    @Bean
    public FilterRegistrationBean filterRegistrationBean() {
        TestFiler testFiler = new TestFiler();
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean(testFiler);

        // 注册需要过滤的 url
        filterRegistrationBean.setUrlPatterns(Arrays.asList("/testServlet"));
        return filterRegistrationBean;
    }

    @Bean
    public ServletListenerRegistrationBean servletListenerRegistrationBean() {
        TestListener testListener = new TestListener();
        return new ServletListenerRegistrationBean(testListener);
    }
}