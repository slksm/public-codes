package com.example.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Update;

import com.example.entity.Admin;

@Mapper
public interface AdminMapper {

    @Select("SELECT * FROM admin WHERE username = #{username,jdbcType=VARCHAR} AND password = #{password,jdbcType=VARCHAR}")
    Admin getByNameAndPassword(Admin admin);

    @Delete("DELETE FROM admin WHERE id = #{id,jdbcType=INTEGER}")
    int deleteByPrimaryKey(Integer id);

    @Insert("INSERT INTO admin ( username, password, age)" +
            "VALUES ( #{username,jdbcType=VARCHAR}, #{password,jdbcType=VARCHAR}, #{age,jdbcType=VARCHAR})")
    int insert(Admin admin);

    @Update("UPDATE admin SET " +
            " username = #{username,jdbcType=VARCHAR},\n" +
            " password = #{password,jdbcType=VARCHAR},\n" +
            " age = #{age,jdbcType=INTEGER}\n" +
            "WHERE id = #{id,jdbcType=INTEGER}")
    int updateByPrimaryKey(Admin admin);
}