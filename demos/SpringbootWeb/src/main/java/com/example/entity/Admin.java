package com.example.entity;


import java.util.Date;

public class Admin {
    private Integer id;
    private String username;
    private String password;
    private Integer age;
    private Date createtime;

    public Admin() {

    }

    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getAge() {
        return age;
    }
    public void setAge(Integer age) {
        this.age = age;
    }

    public Date getCreatetime() {
        return createtime;
    }
    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    @Override
    public String toString() {
        return "Admin {" +
                "username = " + username +
                ", password = " + password +
                ", age = " + age +
                ", createtime = " + createtime +
                '}';
    }
}
