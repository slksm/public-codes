package com.example.controller;

import java.util.Map;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;

import com.example.entity.Admin;
import com.example.service.AdminService;

@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    AdminService adminService;

    @RequestMapping("/login")
    public String login() {
        return "login";
    }

    @RequestMapping("/login/post")
    public String loginPost(String username, String password,
                        Map<String, Object> map, HttpSession session) {

        if ("admin".equals(username) && "123456".equals(password)) {
            session.setAttribute("loginUser", username);
            return "redirect:/home.html";
        } else {
            System.out.println("UserController -> loginPost(): Invalid username or password");
            map.put("msg", "Invalid username or password");
        }

        return "login";
    }

    @RequestMapping("/auth")
    public String auth() {
        return "auth";
    }

    @RequestMapping("/auth/post")
    public String authPost(Admin admin, Map<String, Object> map, HttpSession session) {

        System.out.println("admin: " + admin);

        Admin loginAdmin = adminService.getByNameAndPassword(admin);
        if (loginAdmin != null) {
            session.setAttribute("loginAdmin", loginAdmin);
            return "redirect:/home";
        } else {
            map.put("msg", "Invalid username or password");
        }

        return "auth";
    }
}
