package com.example.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.jdbc.core.JdbcTemplate;
import com.example.entity.User;
import com.example.exception.PageNotExistException;

import com.mytest.TestService;

@Controller
public class IndexController {
    @Autowired
    User user;
    @Autowired
    JdbcTemplate jdbcTemplate;
    @Autowired
    TestService testService;

    @ResponseBody
    @RequestMapping("/hello")
    public String hello() {
        return "Hello page";
    }

    @RequestMapping("/demo")
    public String demo(Map<String, Object> map) {

        user.setFirstName("Tester");
        user.setAge(21);

        map.put("name", "admin");
        map.put("user", user);
        return "demo";
    }

    @RequestMapping("/home")
    public String home() {
        return "home";
    }

    @ResponseBody
    @RequestMapping("/test")
    public String test(String action) {

        if ("error".equals(action)) {
            throw new PageNotExistException();
        }
        return "Test";
    }

    @ResponseBody
    @RequestMapping("/stat")
    public String stat() {
        String SQL = "SELECT count(*) from `user`";
        Integer integer = jdbcTemplate.queryForObject(SQL, Integer.class);
        return integer.toString();
    }

    @ResponseBody
    @RequestMapping("/mytest")
    public String mytest() {
        return testService.showTest("< 自定义starter >");
    }
}
