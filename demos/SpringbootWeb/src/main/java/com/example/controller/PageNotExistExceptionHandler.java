
package com.example.controller;

import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.example.exception.PageNotExistException;

//@ControllerAdvice
public class PageNotExistExceptionHandler {
    @ExceptionHandler(PageNotExistException.class)
    public String handleException(Exception e, HttpServletRequest request) {

        Map<String, Object> map = new HashMap<>();
        request.setAttribute("javax.servlet.error.status_code", 404);
        map.put("code", "PageNotExist");
        map.put("message", e.getMessage());
        request.setAttribute("ext", map);
        return "forward:/error";
    }
}
