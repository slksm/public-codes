package com.example.exception;

public class PageNotExistException extends RuntimeException {
    public PageNotExistException() {
        super("页面不存在");
    }
}

