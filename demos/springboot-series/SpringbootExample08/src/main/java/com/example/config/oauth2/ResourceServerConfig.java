package com.example.config.oauth2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;

@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

    @Autowired
    private TokenStore tokenStore;

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {

        //resources.tokenStore(tokenStore);
        resources.tokenStore(tokenStore).resourceId("rids_1");
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        /*
            注意：

            1. 必须先加上： .requestMatchers().antMatchers(...)，表示对资源进行保护，也就是说，在访问前要进行OAuth认证。
            2. 接着：访问受保护的资源时，要具有相关权限。

            否则，请求只是被 Security 的拦截器拦截，请求根本到不了 OAuth2 的拦截器。

            requestMatchers() 部分说明：

                mvcMatcher(String)}, requestMatchers(), antMatcher(String), regexMatcher(String), and requestMatcher(RequestMatcher).
        */

        http
            // Since we want the protected resources to be accessible in the UI as well we need
            // session creation to be allowed (it's disabled by default in 2.0.6)
            // 如果不设置 session，那么通过浏览器访问被保护的任何资源时，每次是不同的 SessionID，并且将每次请求的历史都记录在 OAuth2Authentication 的 details 中
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
            .and()
            .requestMatchers()
            .antMatchers("/user","/private/**")
            .and()
            .authorizeRequests()
            .antMatchers("/user","/private/**")
            .authenticated();
    }
}
