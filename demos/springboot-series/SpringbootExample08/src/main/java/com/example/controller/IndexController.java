package com.example.controller;

import java.security.Principal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class IndexController {
    @ResponseBody
    @RequestMapping("/test")
    public String test() {
        return "Test Page";
    }

    @RequestMapping("/user")
    @ResponseBody
    public Principal getUser(Principal principal) {
        // principal 被 security 拦截后，是 org.springframework.security.authentication.UsernamePasswordAuthenticationToken
        // 被 OAuth2 拦截后，是 OAuth2Authentication
        return principal;
    }
}
