<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Demo</title>
</head>
<body>
    <h4>Demo Page</h4>

    <c:if test="${not empty msg}">
        <p>Message: ${msg}</p>
    </c:if>

</body>
</html>