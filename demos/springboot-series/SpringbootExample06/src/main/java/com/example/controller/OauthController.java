package com.example.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;

@Controller
@RequestMapping("/oauth")
public class OauthController {
    @RequestMapping("/test/code")
    public String testCode() {
        return "redirect:/oauth/authorize?client_id=1&redirect_uri=/oauth/test/code/callback&response_type=code";
    }

    @RequestMapping("/test/code/callback")
    public String testCodeCallback(@RequestParam String code, Model model) {
        model.addAttribute("code", code);
        return "authorization_code";
    }

    @RequestMapping("/test/implicit")
    public String testImplicit() {
        return "redirect:/oauth/authorize?client_id=2&client_secret=xGJoD2i2lj&grant_type=implicit&response_type=token";
    }

    @RequestMapping("/test/implicit/callback")
    public String testImplicitCallback(HttpServletRequest request) {
        return "implicit";
    }

    @RequestMapping("/test/password")
    public String testPassword() {
        return "password";
    }

    @RequestMapping("/test/client")
    public String testClient() {
        return "client";
    }
}
