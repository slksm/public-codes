package com.example.config.oauth2;

import org.springframework.context.annotation.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.provider.token.TokenStore;

@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter  {
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private TokenStore tokenStore;

    @Override
    public void configure(AuthorizationServerSecurityConfigurer authServer) {
        // 访问权限控制
        authServer.tokenKeyAccess("permitAll()")
                .checkTokenAccess("isAuthenticated()")
                .allowFormAuthenticationForClients();
    }

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {

        // 使内存模式
        clients.inMemory().withClient("1")
                            .secret("{noop}4eti4hAaux")
                            .authorizedGrantTypes("authorization_code", "refresh_token")
                            .scopes("All")
                            .autoApprove(true)
                            .redirectUris("/oauth/test/code/callback")
                            .and()
                            .withClient("2")
                            .secret("{noop}xGJoD2i2lj")
                            .authorizedGrantTypes("implicit")
                            .scopes("All")
                            .autoApprove(true)
                            .redirectUris("/oauth/test/implicit/callback")
                            .and()
                            .withClient("3")
                            .secret("{noop}2lo2ijxJ3e")
                            .authorizedGrantTypes("password", "client_credentials")
                            .scopes("All")
                            .autoApprove(true);
    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) {

        endpoints.authenticationManager(authenticationManager);
        endpoints.tokenStore(tokenStore);
        
    }

}
