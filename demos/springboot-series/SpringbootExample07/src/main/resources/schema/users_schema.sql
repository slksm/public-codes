CREATE TABLE `users` (
    `username` varchar(50) NOT NULL,
    `password` varchar(50) NOT NULL,
    `enabled` tinyint(4) NOT NULL DEFAULT '1',
    PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `user_authorities` (
    `authority_id` int(11) NOT NULL AUTO_INCREMENT,
    `username` varchar(50) NOT NULL,
    `authority` varchar(100) NOT NULL,
    PRIMARY KEY (`authority_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `users` VALUES ('admin', '123456', '1'),('user', '123456', '1');
INSERT INTO `user_authorities` VALUES ('1', 'admin', 'ROLE_ADMIN'),('2', 'user', 'ROLE_USER');