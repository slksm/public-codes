package com.example.controller;

import javax.servlet.http.HttpSession;

import org.springframework.ui.Model;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class IndexController {

    @ResponseBody
    @RequestMapping("/test")
    public String test() {
        return "Test Page";
    }

    @RequestMapping("/home")
    public String home(HttpSession httpSession, Model model) {

        String message = (String) httpSession.getAttribute("message");
        if (message != null && !message.isEmpty()) {
            model.addAttribute("message", message);
            httpSession.removeAttribute("message");
        }

        return "home";
    }

}
