package com.example.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.ui.Model;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class UserController {

    @RequestMapping("/login")
    public String login(HttpServletRequest request, Model model) {

        Object loginUser = request.getSession().getAttribute("loginUser");
        if (loginUser != null)
            return "redirect:/home";

        if ("POST".equals(request.getMethod())) {
            String username = request.getParameter("username");
            String password = request.getParameter("password");

            if ("admin".equals(username) &&
                "123456".equals(password)) {

                request.getSession().setAttribute("loginUser", username);

                request.getSession().setAttribute("message", "Welcome " + username);
                return "redirect:/home";
            }

            model.addAttribute("message", "Invalid username or password");
        }

        return "login";
    }

    @RequestMapping("/logout")
    public String login(HttpSession session) {
        session.removeAttribute("loginUser");
        return "redirect:/login";
    }

}
