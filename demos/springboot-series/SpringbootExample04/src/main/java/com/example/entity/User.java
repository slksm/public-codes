package com.example.entity;

import java.util.Date;

public class User {
    private Integer id;
    private String username;
    private String password;
    private Integer age;
    private Date createtime;

    public User() {

    }

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }
    public void setName(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getAge() {
        return age;
    }
    public void setAge(Integer age) {
        this.age = age;
    }

    public Date getCreatetime() {
        return createtime;
    }
    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    @Override
    public String toString() {
        return "User {" +
                "username = " + username +
                ", password = " + password +
                ", age = " + age +
                ", createtime = " + createtime +
                '}';
    }
}

