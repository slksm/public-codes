package com.example.controller;

import java.sql.SQLException;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.ui.Model;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.entity.User;
import com.example.mapper.UserMapper;

@Controller
public class IndexController {
    @Autowired
    DataSource dataSource;
    @Autowired
    private UserMapper userMapper;

    @ResponseBody
    @RequestMapping("/test")
    public String test() {
        return "Test page";
    }

    @RequestMapping("/home")
    public String home(Model model) {
        model.addAttribute("message", "Spring Boot Login Demo");
        return "home";
    }

    @RequestMapping("/login")
    public String login() {
        return "login";
    }

    @ResponseBody
    @RequestMapping("/druid")
    public String druid() throws SQLException {
        String str = "Druid Page<br><br>";
        str += "DataSource：" + dataSource.getClass() + "<br>";
        str += "DataSource Connection：" + dataSource.getConnection() + "<br>";
        return str;
    }

    @ResponseBody
    @RequestMapping("/mybatis")
    public String mybatis() throws SQLException {
        User user = userMapper.getUserByName("test");
        String str = "MyBatis Page<br><br>";
        str += user + "<br>";
        return str;
    }
}
