package com.example.controller;

import org.springframework.ui.Model;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class IndexController {
    @ResponseBody
    @RequestMapping("/test")
    public String test() {
        return "Test Page";
    }

    @RequestMapping("/home")
    public String home(Model model) {
        model.addAttribute("name", "admin");
        return "home";
    }

}
