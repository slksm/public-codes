package com.example.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.entity.Product;

public interface ProductJpaRepository extends JpaRepository<Product, Integer> {

    //
}