package com.example.dao;

import org.springframework.data.repository.CrudRepository;
import com.example.entity.Product;

public interface ProductCrudRepository extends CrudRepository<Product, Integer> {

    //

}