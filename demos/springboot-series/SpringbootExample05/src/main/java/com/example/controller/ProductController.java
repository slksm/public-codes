package com.example.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.util.StringUtils;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.example.entity.Product;
import com.example.dao.ProductJpaRepository;

@Controller
public class ProductController {
    @Autowired
    private ProductJpaRepository productJpaRepository;

    @RequestMapping(value = "/product", method = RequestMethod.GET)
    public String product(HttpServletRequest request, Model model) {

        Integer index = 1;
        Integer step = 5;
        String strIndex = request.getParameter("index");
        if (StringUtils.hasText(strIndex)) {
            index = Integer.valueOf(strIndex);
            if (index < 1)
                index = 1;
        }
        String strStep = request.getParameter("step");
        if (StringUtils.hasText(strStep)) {
            step = Integer.valueOf(strStep);
            if (step < 5)
                step = 5;
        }

        Sort sort = Sort.by(Sort.Direction.ASC, "id");
        Pageable pageable= PageRequest.of(index-1, step, sort );
        Page<Product> pageData = productJpaRepository.findAll(pageable);

        model.addAttribute("pageData", pageData);

        return "product/index";
    }
}
