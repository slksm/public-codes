package com.example.ws;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

@Component
public class WSTextHandler extends TextWebSocketHandler {

    private List<WebSocketSession> clientSessions = new CopyOnWriteArrayList<>();

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        System.out.println("WSTextHandler -> afterConnectionEstablished(): session.getId() = " + session.getId());

        clientSessions.add(session);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        System.out.println("WSTextHandler -> afterConnectionClosed(): session.getId() = "
                        + session.getId() + ", status.getCode() = " + status.getCode());

        clientSessions.remove(session);
    }

    @Override
    public void handleMessage(WebSocketSession session, WebSocketMessage<?> message) throws Exception {
        System.out.println("WSTextHandler -> handleMessage() -> message = "
                            + message.getPayload().toString());

        Map<String, Object> retMap = new HashMap<>();

        JSONObject recvJson = (JSONObject) JSON.parse(message.getPayload().toString());
        String opt = recvJson.getString("operation");

        if ("command".equals(opt)) {

            String msg = recvJson.getString("message");

            if (msg == null || msg.isEmpty()) {
                retMap.put("ret", "error");
                retMap.put("description", "Invalid command format");
                session.sendMessage(new TextMessage(JSON.toJSON(retMap).toString()));
                return;
            }

            retMap.put("ret", "data");
            retMap.put("message", msg + " (Reply from server)");
            session.sendMessage(new TextMessage(JSON.toJSON(retMap).toString()));

        } else if ("close".equals(opt)) {

            retMap.put("ret", "finish");
            retMap.put("description", "Finish directly");
            session.sendMessage(new TextMessage(JSON.toJSON(retMap).toString()));

        } else {

            retMap.put("ret", "error");
            retMap.put("description", "Invalid data format");
            session.sendMessage(new TextMessage(JSON.toJSON(retMap).toString()));

        }

    }
}
