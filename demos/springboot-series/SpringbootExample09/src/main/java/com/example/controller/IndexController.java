package com.example.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class IndexController {
    @ResponseBody
    @RequestMapping("/test")
    public String test() {
        return "Test Page";
    }

    @ResponseBody
    @RequestMapping("/api/v1/user")
    public String user() {
        return "API v1 user";
    }

    @RequestMapping("/get/resource")
    public String getRes() {
        return "get_res";
    }
}
