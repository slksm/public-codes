package com.example;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.example.service.PersonService;
import com.example.service.PersonServiceImpl;

@Configuration
public class AppConfig {

    /**
     * 相当于 <bean id="personService" class="com.example.service.PersonServiceImpl"></bean>
     */
    @Bean
    public PersonService personService() {
        return new PersonServiceImpl();
    }

}
