package com.example.entity;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import com.example.CustomPropertySourceFactory;

@Component("person4")
@ConfigurationProperties(prefix = "person4")
@PropertySource(value = "classpath:person4.yml", factory = CustomPropertySourceFactory.class)
public class Person4 {
    private String firstName;
    private Integer age;
    private Boolean male;
    private Date birth;
    private Map<String, Object> maps;
    private List<Object> lists;
    private Dog dog;
    public Person4() {
    }
    public Person4(String firstName, Integer age, Boolean male, Date birth,
                   Map<String, Object> maps, List<Object> lists, Dog dog) {
        this.firstName = firstName;
        this.age = age;
        this.male = male;
        this.birth = birth;
        this.maps = maps;
        this.lists = lists;
        this.dog = dog;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public Integer getAge() {
        return age;
    }
    public void setAge(Integer age) {
        this.age = age;
    }
    public Boolean getMale() {
        return male;
    }
    public void setMale(Boolean male) {
        this.male = male;
    }
    public Date getBirth() {
        return birth;
    }
    public void setBirth(Date birth) {
        this.birth = birth;
    }
    public Map<String, Object> getMaps() {
        return maps;
    }
    public void setMaps(Map<String, Object> maps) {
        this.maps = maps;
    }
    public List<Object> getLists() {
        return lists;
    }
    public void setLists(List<Object> lists) {
        this.lists = lists;
    }
    public Dog getDog() {
        return dog;
    }
    public void setDog(Dog dog) {
        this.dog = dog;
    }
    @Override
    public String toString() {
        return "Person4 {" +
                "firstName = " + firstName +
                ", age = " + age +
                ", male = " + male +
                ", birth = " + birth +
                ", maps = " + maps +
                ", lists = " + lists +
                ", dog = " + dog +
                '}';
    }
}