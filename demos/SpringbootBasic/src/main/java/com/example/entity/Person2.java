package com.example.entity;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component("person2")
public class Person2 {
    @Value("${person.firstName}")
    private String firstName;
    @Value("${person.age}")
    private Integer age;
    @Value("${person.male}")
    private Boolean male;
    @Value("${person.birth}")
    private Date birth;
    private Map<String, Object> maps;
    private List<Object> lists;
    private Dog dog;

    public Person2() {
    }
    public Person2(String firstName, Integer age, Boolean male, Date birth,
                   Map<String, Object> maps, List<Object> lists, Dog dog) {
        this.firstName = firstName;
        this.age = age;
        this.male = male;
        this.birth = birth;
        this.maps = maps;
        this.lists = lists;
        this.dog = dog;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public Integer getAge() {
        return age;
    }
    public void setAge(Integer age) {
        this.age = age;
    }
    public Boolean getMale() {
        return male;
    }
    public void setMale(Boolean male) {
        this.male = male;
    }
    public Date getBirth() {
        return birth;
    }
    public void setBirth(Date birth) {
        this.birth = birth;
    }
    public Map<String, Object> getMaps() {
        return maps;
    }
    public void setMaps(Map<String, Object> maps) {
        this.maps = maps;
    }
    public List<Object> getLists() {
        return lists;
    }
    public void setLists(List<Object> lists) {
        this.lists = lists;
    }
    public Dog getDog() {
        return dog;
    }
    public void setDog(Dog dog) {
        this.dog = dog;
    }
    @Override
    public String toString() {
        return "Person2 {" +
                "firstName = " + firstName +
                ", age = " + age +
                ", male = " + male +
                ", birth = " + birth +
                ", maps = " + maps +
                ", lists = " + lists +
                ", dog = " + dog +
                '}';
    }
}
