package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

import com.example.entity.Person;
import com.example.entity.Person2;
import com.example.entity.Person3;
import com.example.entity.Person4;
import com.example.entity.User;

//@ImportResource(locations = {"classpath:/spring-beans.xml"})
@SpringBootApplication
public class App {

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);

        Person person = (Person) ApplicationContextUtils.getById("person");
        System.out.println(person);
/*
        Person2 person2 = (Person2) ApplicationContextUtils.getById("person2");
        System.out.println(person2);

        Person3 person3 = (Person3) ApplicationContextUtils.getById("person3");
        System.out.println(person3);

        Person4 person4 = (Person4) ApplicationContextUtils.getById("person4");
        System.out.println(person4);

        Boolean b = ApplicationContextUtils.containsBean("personService");
        if (b) {
            System.out.println("personService in IOC");
        } else {
            System.out.println("personService not in IOC");
        }
*/

        User user = (User) ApplicationContextUtils.getById("user");
        System.out.println(user);
    }

}



