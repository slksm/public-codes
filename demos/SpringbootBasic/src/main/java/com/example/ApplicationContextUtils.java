package com.example;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/*
 * 在非 SpringBoot 工厂管理的普通类中，如果要获取工厂管理的对象，不能再使用 @Autowired 等注入的注解，
 * SpringBoot 提供了一个 “ApplicationContextAware” 接口，实现此接口，即可获取工厂管理的全部内容。
 */
@Component
public class ApplicationContextUtils implements ApplicationContextAware {
    private static ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    // 通过属性名查询对象
    public static Boolean containsBean(String id) {
          return applicationContext.containsBean(id);
    }

    // 通过属性名获取对象
    public static Object getById(String id){
        Object bean = applicationContext.getBean(id);
        return bean;
    }

    // 通过属性类获取对象
    public static Object getByClass(Class clazz){
        Object bean = applicationContext.getBean(clazz);
        return bean;
    }
    // 通过属性类获取对象
    public static Object getByNameAndClass(String id, Class clazz){
        Object bean = applicationContext.getBean(id, clazz);
        return bean;
    }

}
