package com.example.service;

import org.springframework.beans.factory.annotation.Autowired;
import com.example.entity.Person;

public class PersonServiceImpl implements PersonService {
    @Autowired
    private Person person;
    @Override
    public Person getPersonInfo() {
        return person;
    }
}
