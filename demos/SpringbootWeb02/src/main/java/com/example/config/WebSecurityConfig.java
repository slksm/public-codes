package com.example.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;

@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsService userDetailsService;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // 配置认证
        http.authorizeRequests().anyRequest().authenticated()

            .and()
            .formLogin()
            .loginPage("/login") // 自定义登录页面
            .loginProcessingUrl("/login/post") // 登录访问路径
            .defaultSuccessUrl("/home").permitAll();  // 登陆成功之后跳转地址

            //.and()
            //.csrf(); // 关闭 csrf 保护功能

    }

}
